# Design patterns in Dot.Net

## Creational

### Creational: Singleton

Make a class itself responsible for its instantiation and ensure only one instance of the class ever exists by:

- hiding the constructor
- and exposing single global instance.

Use case:

Hold a global total, to each can be added/substracted.

### Creational: Abstract factory

Groups individual factories that have common theme.  
The individual factories share common base class.  
Can be used to create fake objects for purpose of testing.  

Use case:

Extract first and last name from fill name.

## Behavioral

### Behavioral: Chain of responsibility

Send objects to a series of receivers without senders being concerned which receiver handles the request.

Use case:

Incoming emails to university are routed to appropriate team:

| Keywords           | University Team | Handler                  |
| ------------------ | --------------- | ------------------------ |
| academics          | Academic Team   | `AcademicEmailHandles()` |
| alumni, transcript | Alumni Affairs  | `AlumniEmailHandler()`   |
...

### Behavioral: Memento

Provides ability to keep state of an object without the need to store the entire object. The state can be rolled back.

Consists of three parts:

- Memento - an object that can remember the state of another object
- Originator - an object which state is to remember
  - Has method GetMemento() that returns it's state
  - Has method SetMemento() that receives a state to restore from
- Caretaker - an object that controls when to create and restore Memento state

Use case:

A blog post form that has "Save Copy" and "Undo Changes".

### Behavioral: State

The behavior of object changes accordingly to its internal state.

Consists of two parts:

- Context - a class which behavior will change accordingly to its state is named
- State interface - defines the behavior of the Context
- State implementations - Implements state each differently

Use case:

Car can turn on, turn off, drive, or stop.  
When the car is driving it can only stop, but cannot turn on or off.  
When the car has stopped it can only drive or turn off.  
When the car is turned off, it only can turn on.  

### Behavioral: Command

Encapsulate requests as objects and send to receiver.  
Invoker and receiver are now decoupled.  
Commands can be batched and executed later.  
Gives a possibility to track all the changes and to undo/redo.  
It is very popular in C#, for example when we want to delay or queue execution of a request.  

Use case:

Power on/off switch. The switch manufacturer does not need to know where exactly will be used.

## Structural
