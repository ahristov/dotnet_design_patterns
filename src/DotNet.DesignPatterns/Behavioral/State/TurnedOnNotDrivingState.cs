﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public class TurnedOnNotDrivingState : ICarState
    {
        public CarStateType CurrentState => CarStateType.TurnedOnNotDriving;
        public ICarState Drive() => new DrivingState();
        public ICarState Stop() => this;
        public ICarState TurnOff() => new TurnedOffState();
        public ICarState TurnOn() => this;
    }
}
