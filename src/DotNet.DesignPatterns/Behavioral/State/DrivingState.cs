﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public class DrivingState : ICarState
    {
        public CarStateType CurrentState => CarStateType.Driving;
        public ICarState Drive() => this;
        public ICarState Stop() => new TurnedOnNotDrivingState();
        public ICarState TurnOff() => this;
        public ICarState TurnOn() => this;
    }
}
