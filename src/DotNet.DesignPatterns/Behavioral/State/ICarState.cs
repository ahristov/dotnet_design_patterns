﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public interface ICarState
    {
        CarStateType CurrentState { get; }
        ICarState TurnOn();
        ICarState TurnOff();
        ICarState Stop();
        ICarState Drive();
    }
}
