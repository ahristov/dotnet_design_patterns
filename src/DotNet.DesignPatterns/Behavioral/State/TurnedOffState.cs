﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public class TurnedOffState : ICarState
    {
        public CarStateType CurrentState => CarStateType.TurnedOff;
        public ICarState Drive() => this;
        public ICarState Stop() => this;
        public ICarState TurnOff() => this;
        public ICarState TurnOn() => new TurnedOnNotDrivingState();
    }
}
