﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public class Car
    {
        private ICarState _state;

        public Car()
        {
            _state = new TurnedOffState();
        }

        public CarStateType CurrentState => _state.CurrentState;
        public void TurnOn() => _state = _state.TurnOn();
        public void TurnOff() => _state = _state.TurnOff();
        public void Stop() => _state = _state.Stop();
        public void Drive() => _state = _state.Drive();
    }
}
