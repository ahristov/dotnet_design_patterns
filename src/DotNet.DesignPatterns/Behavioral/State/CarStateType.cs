﻿namespace DotNet.DesignPatterns.Behavioral.State
{
    public enum CarStateType
    {
        TurnedOff,
        TurnedOnNotDriving,
        Driving,
    }
}
