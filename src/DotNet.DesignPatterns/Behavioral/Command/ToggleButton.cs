﻿namespace DotNet.DesignPatterns.Behavioral.Command
{
    public class ToggleButton : IToggleButton
    {
        private Lamp _lamp;
        public void AttachToLamp(Lamp lamp) => _lamp = lamp;

        public void Toggle()
        {
            if (_lamp == null)
                throw new ToggleButtonNotAttachedException();

            if (_lamp.LightIsOn)
                _lamp.TurnLightOff();
            else
                _lamp.TurnLightOn();
        }
    }
}
