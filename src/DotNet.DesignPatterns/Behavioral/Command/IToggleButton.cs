﻿namespace DotNet.DesignPatterns.Behavioral.Command
{
    public interface IToggleButton
    {
        void Toggle();
    }
}
