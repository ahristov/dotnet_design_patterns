﻿namespace DotNet.DesignPatterns.Behavioral.Command
{
    public class Lamp
    {
        public bool LightIsOn { get; private set; }

        public void TurnLightOn()
        {
            LightIsOn = true;
        }
        public void TurnLightOff()
        {
            LightIsOn = false;
        }

    }
}
