﻿using System;

namespace DotNet.DesignPatterns.Behavioral.Command
{
    public class ToggleButtonNotAttachedException : Exception
    {
        public ToggleButtonNotAttachedException()
            : base("Lamp switch not attached")
        { }
    }
}
