﻿using DotNet.DesignPatterns.Behavioral.Memento;

namespace DotNet.DesignPatterns.UnitTests.Behavioral.Memento
{
    public class BlogPostForm : IOriginator<BlogPostFormState>
    {
        private BlogPostFormState _state = new BlogPostFormState();

        public string BlogTitleInputField { get => _state.BlogTitle; set => _state.BlogTitle = value; }
        public string BlogContentInputField { get => _state.BlogContent; set => _state.BlogContent = value; }

        public Memento<BlogPostFormState> GetMemento() => new Memento<BlogPostFormState>().Save(_state);

        public void SetMemento(Memento<BlogPostFormState> memento) => _state = memento.Restore() ?? _state;
    }
}
