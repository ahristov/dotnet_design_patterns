﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.Memento
{
    [Serializable]
    public class BlogPostFormState
    {
        public const string TITLE_PLACEHOLDER_HINT = "Please enter blog post title";
        public const string CONTENT_PLACEHOLDER_HINT = "Please enter blog post content";

        public string BlogTitle { get; set; } = TITLE_PLACEHOLDER_HINT;
        public string BlogContent { get; set; } = CONTENT_PLACEHOLDER_HINT;
        public List<string> Tags { get; set; } = new List<string>();
    }
}
