﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DotNet.DesignPatterns.Behavioral.Memento
{
    public class Memento<TState> : IDisposable
    {
        private bool _isDisposed;
        MemoryStream _stream = new MemoryStream();
        BinaryFormatter _formatter = new BinaryFormatter();

        public Memento<TState> Save(TState state)
        {
            _formatter.Serialize(_stream, state);
            return this;
        }

        public TState Restore()
        {
            if (_stream.CanSeek == false || _stream.Length == 0)
                return default(TState);

            _stream.Seek(0, SeekOrigin.Begin);
            TState state = (TState)_formatter.Deserialize(_stream);

            return state;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }

            _isDisposed = true;
        }
    }
}
