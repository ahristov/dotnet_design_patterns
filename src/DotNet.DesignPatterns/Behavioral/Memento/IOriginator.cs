﻿namespace DotNet.DesignPatterns.Behavioral.Memento
{
    public interface IOriginator<TState>
    {
        Memento<TState> GetMemento();
        void SetMemento(Memento<TState> memento);

    }
}
