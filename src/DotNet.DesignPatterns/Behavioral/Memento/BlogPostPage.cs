﻿using DotNet.DesignPatterns.UnitTests.Behavioral.Memento;

namespace DotNet.DesignPatterns.Behavioral.Memento
{
    public class BlogPostPage
    {
        Memento<BlogPostFormState> _memento = new Memento<BlogPostFormState>();
        internal BlogPostForm Form = new BlogPostForm();

        public void SaveDraft() => _memento = Form.GetMemento();

        public void UndoChanges() => Form.SetMemento(_memento);
    }
}
