﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public abstract class BaseEmailHandler : IUniversityEmailHandler
    {
        protected abstract ICollection<string> Keywords { get; }

        IUniversityEmailHandler _nextHandler;
        public IUniversityEmailHandler SetNextEmailHandler(IUniversityEmailHandler nextHandler)
        {
            _nextHandler = nextHandler;
            return _nextHandler;
        }

        protected abstract Type ProcessEmailHandlerFinal(string emailText);

        public Type ProcessEmailHandler(string emailText)
        {
            bool keyWordFound = Keywords.Count == 0
                || Keywords.Any(x => emailText.Contains(x));

            if (keyWordFound)
            {
                return ProcessEmailHandlerFinal(emailText);
            }
            else
            {
                return (_nextHandler != null)
                  ? _nextHandler.ProcessEmailHandler(emailText)
                  : null;
            }
        }
    }
}
