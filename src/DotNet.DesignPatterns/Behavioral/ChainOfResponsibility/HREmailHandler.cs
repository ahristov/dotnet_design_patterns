﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class HREmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new[] { "career", "job", "faculty" };

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            Console.WriteLine("The HR Team processed this email");
            return this.GetType();
        }
    }
}   