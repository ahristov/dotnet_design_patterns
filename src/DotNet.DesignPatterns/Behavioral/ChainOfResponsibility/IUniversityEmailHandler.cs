﻿using System;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public interface IUniversityEmailHandler
    {
        IUniversityEmailHandler SetNextEmailHandler(IUniversityEmailHandler nextHandler);
        Type ProcessEmailHandler(string emailText);
    }
}
