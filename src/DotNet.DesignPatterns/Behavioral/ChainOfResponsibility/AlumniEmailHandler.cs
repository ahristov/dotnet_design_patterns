﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class AlumniEmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new[] { "alumni", "transcript" };

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            System.Console.WriteLine("The Alumni Team processed this email");
            return this.GetType();
        }
    }
}
