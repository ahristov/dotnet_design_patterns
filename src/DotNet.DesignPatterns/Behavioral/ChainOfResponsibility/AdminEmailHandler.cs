﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class AdminEmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new string[] {};

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            Console.WriteLine("The Admin Team processed this email");
            return this.GetType();
        }
    }
}