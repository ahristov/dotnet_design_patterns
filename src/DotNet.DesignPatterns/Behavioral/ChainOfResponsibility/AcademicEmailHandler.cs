﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class AcademicEmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new[] { "academic" };

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            Console.WriteLine("The Academic Team processed this email");
            return this.GetType();
        }
    }
}
