﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class FinanceEmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new[] { "financial", "student aid", "tuition" };

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            Console.WriteLine("The Finance Team processed this email");
            return this.GetType();
        }
    }
}