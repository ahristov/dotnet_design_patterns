﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class AdvisingEmailHandler : BaseEmailHandler
    {
        protected override ICollection<string> Keywords => new[] { "advising", "schedule", "course" };

        protected override Type ProcessEmailHandlerFinal(string emailText)
        {
            Console.WriteLine("The Advising Team processed this email");
            return this.GetType();
        }
    }
}
