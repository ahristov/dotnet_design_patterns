﻿using System;

namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public class FullNameFactory : AbstractFactory<FullNameBase>
    {
        public const string WESTERN_ORDER = "WesternOrder";
        public const string EASTERN_ORDER = "EasternOrder";
        public const string EASTERN_OFFICIAL_ORDER = "EasternOfficialOrder";

        private static readonly FullNameFactory _Factory;
        
        private FullNameFactory() { }

        static FullNameFactory()
        {
            _Factory = new FullNameFactory();
            _Factory.Register(WESTERN_ORDER, typeof(WesternFullName));
            _Factory.Register(EASTERN_ORDER, typeof(EasternFullName));
            _Factory.Register(EASTERN_OFFICIAL_ORDER, typeof(EasternOfficialFullName));
        }

        public static new FullNameFactory CreateFactory() => _Factory;

        public FullNameBase CreateInstance(string key, string firstName, string lastName) 
            => CreateInstance(key, 
                new Type[] { typeof(string), typeof(string) },
                new object[] { firstName, lastName });
    }
}
