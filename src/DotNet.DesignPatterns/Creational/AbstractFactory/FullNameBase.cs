﻿namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public abstract class FullNameBase
    {
        protected string FirstName { get; }
        protected string LastName { get; }

        public FullNameBase(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        // Full name can be composed differently 
        // as the different societies have they different 
        // naming conventions, see: http://en.wikipedia.org/wiki/Full_name#Naming_convention
        public abstract string GetFullName();
    }
}
