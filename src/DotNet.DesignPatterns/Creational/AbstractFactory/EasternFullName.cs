﻿namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public class EasternFullName : FullNameBase
    {
        public EasternFullName(string firstName, string lastName)
            : base(firstName, lastName) { }

        public override string GetFullName() => $"{LastName} {FirstName}";
    }
}
