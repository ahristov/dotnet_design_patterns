﻿using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public abstract class AbstractFactory<T>
    {
        public static AbstractFactory<T> CreateFactory()
        {
            throw new Exception($"{nameof(AbstractFactory)}.{nameof(CreateFactory)} has to be overridden");
        }

        private static readonly Dictionary<string, Type> _registered = new Dictionary<string, Type>();

        protected void Register(string key, Type type) => _registered.Add(key, type);

        protected T CreateInstance(string key, Type[] paramTypes, object[] paramValues)
        {
            Type type = _registered[key];

            System.Reflection.ConstructorInfo ctor = type.GetConstructor(paramTypes);

            return (T)ctor.Invoke(paramValues);
        }
    }
}
