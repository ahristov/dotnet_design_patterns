﻿namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public class WesternFullName : FullNameBase
    {
        public WesternFullName(string firstName, string lastName)
            : base(firstName, lastName) { }

        public override string GetFullName() => $"{FirstName} {LastName}";
    }
}
