﻿namespace DotNet.DesignPatterns.Creational.AbstractFactory
{
    public class EasternOfficialFullName : FullNameBase
    {
        public EasternOfficialFullName(string firstName, string lastName)
            : base(firstName, lastName) { }

        public override string GetFullName() => $"{LastName.ToUpperInvariant()} {FirstName}";
    }
}
