﻿namespace DotNet.DesignPatterns.Creational.Singleton
{
    public sealed class GlobalTotalSingleton
    {
        public static GlobalTotalSingleton Instance { get; } = new GlobalTotalSingleton();
        public int Total { get; private set; }

        public void Add(int val) => Total += val;


        private GlobalTotalSingleton() { }
        internal static void ResetTotal() => Instance.Total = 0;
    }
}
