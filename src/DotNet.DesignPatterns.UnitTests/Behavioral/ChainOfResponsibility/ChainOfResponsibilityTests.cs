﻿using DotNet.DesignPatterns.Behavioral.ChainOfResponsibility;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.UnitTests.Behavioral.ChainOfResponsibility
{
    [TestFixture]
    public class ChainOfResponsibilityTests
    {
        [TestCaseSource(nameof(TestsDataSetHandledBySpecificTeams))]
        public void RunTestsHandledBySpecificTeams(TestData testData) => RunTest(testData);

        [TestCaseSource(nameof(TestsDataSetHandledByAdmin))]
        public void RunTestsHandledByAdmin(TestData testData) => RunTest(testData);

        private void RunTest(TestData testData)
        {
            Console.WriteLine($"Running test: {testData.TestName}");

            Type processedHandlerType = testData.ChainToTest.ProcessEmailHandler(testData.EmailText);

            processedHandlerType.Should().NotBeNull();
            processedHandlerType.Should().Be(testData.ExpectedHandlerType);
        }

        private static IEnumerable<TestCaseData> TestsDataSetHandledBySpecificTeams
        {
            get
            {
                string testName;

                var sut = new AcademicEmailHandler();
                sut
                    .SetNextEmailHandler(new AlumniEmailHandler())
                    .SetNextEmailHandler(new AdvisingEmailHandler())
                    .SetNextEmailHandler(new FinanceEmailHandler())
                    .SetNextEmailHandler(new HREmailHandler())
                    .SetNextEmailHandler(new AdminEmailHandler());

                testName = "Academic Team receives emails with keyword \"academic\"";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "Interested in academic ...",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(AcademicEmailHandler),
                })
                    .SetName(testName);

                testName = "Advising Team receives emails with keyword \"course\"";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "Needed information about the course ...",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(AdvisingEmailHandler),
                })
                    .SetName(testName);

                testName = "Finance Team receives emails with keyword \"tuition\"";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "Needed information about the tuition ...",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(FinanceEmailHandler),
                })
                    .SetName(testName);
            }
        }


        private static IEnumerable<TestCaseData> TestsDataSetHandledByAdmin
        {
            get
            {
                string testName;
                AcademicEmailHandler sut;

                sut = new AcademicEmailHandler();
                sut
                    .SetNextEmailHandler(new AlumniEmailHandler())
                    .SetNextEmailHandler(new AdvisingEmailHandler())
                    .SetNextEmailHandler(new FinanceEmailHandler())
                    .SetNextEmailHandler(new HREmailHandler())
                    .SetNextEmailHandler(new AdminEmailHandler());

                testName = "Admin Team receives all unsorted emails";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "This email was not sorted to specific handler",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(AdminEmailHandler),
                })
                    .SetName(testName);

                testName = "Admin Team receives all empty emails";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "This email was not sorted to specific handler",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(AdminEmailHandler),
                })
                    .SetName(testName);

                //

                sut = new AcademicEmailHandler();
                sut
                    .SetNextEmailHandler(new AlumniEmailHandler())
                    .SetNextEmailHandler(new AdvisingEmailHandler())
                    .SetNextEmailHandler(new AdminEmailHandler()) // It is before Finance.
                    .SetNextEmailHandler(new FinanceEmailHandler())
                    .SetNextEmailHandler(new HREmailHandler());

                testName = "Admin email handler ends chain or responsibility execution";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    EmailText = "Needed information about the tuition ...",
                    ChainToTest = sut,
                    ExpectedHandlerType = typeof(AdminEmailHandler),
                })
                    .SetName(testName);

            }
        }
        public class TestData
        {
            public string TestName { get; set; }
            public string EmailText { get; set; }
            public IUniversityEmailHandler ChainToTest { get; set; }
            public Type ExpectedHandlerType { get; set; }
        }
    }
}
