﻿using DotNet.DesignPatterns.Behavioral.Command;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.UnitTests.Behavioral.Command
{
    public class CommandTests
    {
        [TestCaseSource(nameof(TestDataSet))]
        public void RunTest(TestData testData)
        {
            Console.WriteLine($"Running test: {testData.TestName}");

            var lamp = new Lamp();
            var onOffSwitch = new ToggleButton();
            onOffSwitch.AttachToLamp(lamp);

            testData.TestAction(onOffSwitch);

            lamp.LightIsOn.Should().Be(testData.ExpectedLignStatus);
        }

        private static IEnumerable<TestCaseData> TestDataSet
        {
            get
            {
                string testName;

                testName = "Lamp has initial state turned off";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    TestAction = (ToggleButton onOffSwitch) => { },
                    ExpectedLignStatus = false,
                })
                    .SetName(testName);

                testName = "Given lamp is off, after toggling once, lamp is on";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    TestAction = (ToggleButton onOffSwitch) => {
                        onOffSwitch.Toggle();
                        return;
                    },
                    ExpectedLignStatus = true,
                })
                    .SetName(testName);

                testName = "Given lamp is off, after toggling twice, lamp is off";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    TestAction = (ToggleButton onOffSwitch) => {
                        onOffSwitch.Toggle();
                        onOffSwitch.Toggle();
                        return;
                    },
                    ExpectedLignStatus = false,
                })
                    .SetName(testName);

            }
        }

        public class TestData
        {
            public string TestName { get; set; }
            public Action<ToggleButton> TestAction { get; set; }
            public bool ExpectedLignStatus { get; set; }
        }
    }
}
