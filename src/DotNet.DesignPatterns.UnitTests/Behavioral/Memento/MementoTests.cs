﻿using DotNet.DesignPatterns.Behavioral.Memento;
using FluentAssertions;
using NUnit.Framework;

namespace DotNet.DesignPatterns.UnitTests.Behavioral.Memento
{
    [TestFixture]
    public class MementoTests
    {
        [TestCase(null, TestName = "Blog post page can save draft and undo changes to saved draft")]
        public void BlogPostPage_CanSaveDraftAndUndoChangesToSavedDraft(object _)
        {
            var sut = new BlogPostPage();

            sut.Form.BlogTitleInputField = "t1";
            sut.Form.BlogContentInputField = "c1";
            sut.SaveDraft();

            sut.Form.BlogTitleInputField = "t2";
            sut.Form.BlogContentInputField = "c2";

            sut.Form.BlogTitleInputField.Should().Be("t2");
            sut.Form.BlogContentInputField.Should().Be("c2");

            sut.UndoChanges();

            sut.Form.BlogTitleInputField.Should().Be("t1");
            sut.Form.BlogContentInputField.Should().Be("c1");

            sut.Form.BlogTitleInputField = "t2";
            sut.Form.BlogContentInputField = "c2";

            sut.Form.BlogTitleInputField.Should().Be("t2");
            sut.Form.BlogContentInputField.Should().Be("c2");

            sut.UndoChanges();

            sut.Form.BlogTitleInputField.Should().Be("t1");
            sut.Form.BlogContentInputField.Should().Be("c1");
        }

        [TestCase(null, TestName = "Blog post page keeps current state on undo if draft was never saved")]
        public void BlogPostPage_WhenDraftNeverSaved_UndoChangesShouldKeepCurrentState(object _)
        {
            var sut = new BlogPostPage();
            sut.UndoChanges();

            sut.Form.BlogTitleInputField.Should().NotBeNullOrEmpty();
            sut.Form.BlogContentInputField.Should().NotBeNullOrEmpty();

            sut.Form.BlogTitleInputField = "t1";
            sut.Form.BlogContentInputField = "c1";
            sut.UndoChanges();

            sut.Form.BlogTitleInputField.Should().Be("t1");
            sut.Form.BlogContentInputField.Should().Be("c1");
            sut.UndoChanges();
            sut.UndoChanges();
            sut.UndoChanges();

            sut.Form.BlogTitleInputField.Should().Be("t1");
            sut.Form.BlogContentInputField.Should().Be("c1");
        }
    }
}
