﻿using DotNet.DesignPatterns.Behavioral.State;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.UnitTests.Behavioral.State
{
    [TestFixture]
    public class StateTests
    {
        [TestCaseSource(nameof(TestDataSet))]
        public void RunTest(TestData testData)
        {
            Console.WriteLine($"Running test: {testData.TestName}");

            var sut = new Car();
            testData.OperateCar(sut);

            sut.CurrentState.Should().Be(testData.ExpectesState);
        }

        private static IEnumerable<TestCaseData> TestDataSet
        {
            get
            {
                string testName;

                testName = "Car has initial state turned off";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) => { },
                    ExpectesState = CarStateType.TurnedOff,
                })
                    .SetName(testName);

                testName = "Turned off car cannot drive";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) =>
                    {
                        car.Drive();
                    },
                    ExpectesState = CarStateType.TurnedOff,
                })
                    .SetName(testName);

                testName = "Turned off car can turn on";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) =>
                    {
                        car.TurnOn();
                    },
                    ExpectesState = CarStateType.TurnedOnNotDriving,
                })
                    .SetName(testName);

                testName = "Turned off car can turn on and then turn off";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) =>
                    {
                        car.TurnOn();
                        car.TurnOff();
                    },
                    ExpectesState = CarStateType.TurnedOff,
                })
                    .SetName(testName);

                testName = "Turned off car can turn on and drive";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) =>
                    {
                        car.TurnOn();
                        car.Drive();
                    },
                    ExpectesState = CarStateType.Driving,
                })
                    .SetName(testName);

                testName = "Turned off car can turn on, drive and then stop";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    OperateCar = (Car car) =>
                    {
                        car.TurnOn();
                        car.Drive();
                        car.Stop();
                    },
                    ExpectesState = CarStateType.TurnedOnNotDriving,
                })
                    .SetName(testName);

            }
        }

        public class TestData
        {
            public string TestName { get; set; }
            public Action<Car> OperateCar { get; set; }
            public CarStateType ExpectesState { get; set; }
        }
    }
}
