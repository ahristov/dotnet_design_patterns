﻿using DotNet.DesignPatterns.Creational.Singleton;
using FluentAssertions;
using NUnit.Framework;

namespace DotNet.DesignPatterns.UnitTests.Creational.Singleton
{
    [TestFixture]
    public class GlobalTotalSingletonTests
    {
        [TestCase(null, TestName = "Singleton maintains global state")]
        public void GlobalTotalSingleton_MaintainsGlobalState(object _)
        {
            GlobalTotalSingleton.ResetTotal();

            GlobalTotalSingleton.Instance.Add(1);
            GlobalTotalSingleton.Instance.Total.Should().Be(1);

            GlobalTotalSingleton.Instance.Add(1);
            GlobalTotalSingleton.Instance.Total.Should().Be(2);

            GlobalTotalSingleton.Instance.Add(2);
            GlobalTotalSingleton.Instance.Total.Should().Be(4);

            GlobalTotalSingleton.Instance.Add(4);
            GlobalTotalSingleton.Instance.Total.Should().Be(8);

            GlobalTotalSingleton.ResetTotal();
            GlobalTotalSingleton.Instance.Total.Should().Be(0);
        }

    }
}
