﻿using DotNet.DesignPatterns.Creational.AbstractFactory;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace DotNet.DesignPatterns.UnitTests.Creational.AbstractFactory
{
    [TestFixture]
    public class FullNameFactoryTests
    {
        const string FIRST_NAME = "John";
        const string LAST_NAME = "Doe";

        [TestCaseSource(nameof(TestDataSetForNameOrder))]
        public void TestForNameOrder(TestData testData) => RunTest(testData);

        [TestCaseSource(nameof(TestDataSetForExpectedException))]
        public void TesttForExpectedException(TestData testData) => RunTest(testData);

        private void RunTest(TestData testData)
        {
            Console.WriteLine($"Running test: {testData.TestName}");

            FullNameBase sut = null;

            Action runSut = () =>
            {
                sut = FullNameFactory.CreateFactory()
                    .CreateInstance(
                        testData.NameConvention,
                        testData.FirstName,
                        testData.LastName);
            };

            if (testData.ExpectedExceptionType != null)
            {
                var ex = Assert.Throws(testData.ExpectedExceptionType, () => runSut());
                ex.Should().NotBeNull();
                ex.GetType().Should().Be(testData.ExpectedExceptionType);
            }
            else
            {
                runSut();
                sut.Should().NotBeNull();

                var fullName = sut.GetFullName();
                fullName.Should().NotBeNullOrEmpty();
                fullName.Should().Be(testData.ExpectedFullName);
            }
        }

        private static IEnumerable<TestCaseData> TestDataSetForExpectedException
        {
            get
            {
                string testName;

                testName = "Throws KeyNotFoundException when name convention is not registered";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    NameConvention = string.Empty,
                    FirstName = FIRST_NAME,
                    LastName = LAST_NAME,
                    ExpectedExceptionType = typeof(KeyNotFoundException),
                })
                    .SetName(testName);

            }
        }
        private static IEnumerable<TestCaseData> TestDataSetForNameOrder
        {
            get
            {
                string testName;

                testName = "Western name order is: FirstName LastName";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    NameConvention = FullNameFactory.WESTERN_ORDER,
                    FirstName = FIRST_NAME,
                    LastName = LAST_NAME,
                    ExpectedFullName = $"{FIRST_NAME} {LAST_NAME}"
                })
                    .SetName(testName);

                testName = "Eastern name order is: LastName FirstName";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    NameConvention = FullNameFactory.EASTERN_ORDER,
                    FirstName = FIRST_NAME,
                    LastName = LAST_NAME,
                    ExpectedFullName = $"{LAST_NAME} {FIRST_NAME}"
                })
                    .SetName(testName);

                testName = "Eastern official name order is: LASTNAME FirstName";
                yield return new TestCaseData(new TestData
                {
                    TestName = testName,
                    NameConvention = FullNameFactory.EASTERN_OFFICIAL_ORDER,
                    FirstName = FIRST_NAME,
                    LastName = LAST_NAME,
                    ExpectedFullName = $"{LAST_NAME.ToUpperInvariant()} {FIRST_NAME}"
                })
                    .SetName(testName);

            }
        }

        public class TestData
        {
            public string TestName { get; set; }
            public string NameConvention { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string ExpectedFullName { get; set; }
            public Type ExpectedExceptionType { get; set; }
        }
    }
}
